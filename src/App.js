import './App.css';
import useCurrentScroll from './components/useCurrentScroll';
import Scroll from './components/VideoScroll';
import Media from "./medias/tests.mp4"

function App() {


  return (
    <div className="App">
      <Scroll frame={useCurrentScroll()} fps={60}>
        <video
          tabIndex="0"
          autobuffer="autobuffer"
          preload="preload"
          style={{ width: '100%', objectFit: 'contain' }}
          playsInline
        >
          <source type="video/mp4" src={Media} />
        </video>
      </Scroll>
    </div>
  );
}

export default App;
