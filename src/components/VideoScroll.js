import React, { useEffect, useState } from "react";
import useCurrentScroll from "./useCurrentScroll";



export default function VideoScroll({ children, frame, fps, ...rest }) {


    const videoRef = React.createRef()
    const divWrapperRef = React.createRef()

    const [isReady, setIsReady] = useState(false)



    const attachRefToVideo = () => {
        return React.cloneElement(children, { ref: videoRef })
    }

    useEffect(() => {
        const _ref = videoRef.current
        _ref.addEventListener('loadedmetadata', () => {
            setIsReady(true)
        })
        if (isReady) {
            const currentFrame = frame / fps
            _ref.currentTime = currentFrame
        }
        return () => {
            _ref.removeEventListener('loadedmetadata', () => {
                console.log(_ref)
            })
        }

    }, [videoRef, frame, isReady, fps])

    return (
        <div ref={divWrapperRef} {...rest}>
            <div style={{ position: "fixed" }}>{attachRefToVideo()}</div>

            <div style={{ position: "fixed", zIndex: 10, color: "white" }}>{useCurrentScroll()}</div>
        </div>
    )
}